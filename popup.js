// Get all html elements
var _loginButton = document.getElementById('button');
var _usernameInput = document.getElementById('login');
var _passwordInput = document.getElementById('password');
var _loadingIndicator = document.getElementById('loadingindicator');

// Used Variables
var _username = "";
var _password = "";

// Setup listeners
document.getElementById("LogoutButton").addEventListener('click', function(event){ OnLogout_Click(event); });
_loginButton.addEventListener('click', function(event){ OnLogin_Click(); });

// Gets the credentials from storage and checks if its possible to login
LoadCredentials((succeeded) =>
{
	if (succeeded) {
		FioCheckCredentials(_username, _password, (success) =>{
			if(success){
				DisplayUserPage();
			}else{
				DisplayLoginPage();
			}
		});
	}
	else {
		DisplayLoginPage();
	}
});

function OnLogin_Click(){
	_username = _usernameInput.value;
	_password = _passwordInput.value;

	SetLoadingIndicator(true);

	Login(_username, _password, (success) =>
		{
			SetLoadingIndicator(false);
			if(success){
				window.close();
			}else{
				alert("Login was not successful");
			}
		});
}

function OnLogout_Click(event){
	if(event.type == "click"){
		chrome.storage.local.remove([
			'username',
			'password'],
			function(result) {});

		_password.value = "";
	
		DisplayLoginPage();
		alert("Successfully logged out");
	}
}

function SetLoadingIndicator(isLoading)
{
	if(isLoading){
		_loginButton.disabled = true;
		_loadingIndicator.style.visibility = "visible";
	}else{
		_loginButton.disabled = false;
		_loadingIndicator.style.visibility = "hidden";
	}
}


// Display the login page (only if user authentication fails)
function DisplayLoginPage()
{
	document.getElementById('LoginPage').style.visibility = "visible";
	document.getElementById('LoginPage').style.maxHeight = "";

	document.getElementById('UserPage').style.visibility = "hidden";
	document.getElementById('UserPage').style.maxHeight = "0px";
}

// Displays the logged in user data
function DisplayUserPage()
{
	document.getElementById('UserPage').style.visibility = "visible";
	document.getElementById('UserPage').style.maxHeight = "";

	document.getElementById('LoginPage').style.visibility = "hidden";
	document.getElementById('LoginPage').style.maxHeight = "0px";
	
	document.getElementById('GreetingsText').innerText = "Welcome " + _username;
	document.getElementById('InfoText').innerText = "You are already logged in. Everything should work as expected";
	SetLoadingIndicator(false);
}

function Login(username, password){
	FioCheckCredentials(username, password, (success) =>{
		if(success){
			DisplayUserPage();
			SaveCredentials(username, password);
		}else{
			alert("Login was not successful");
			chrome.storage.local.clear(function(result){});
			SetLoadingIndicator(false);
		}
	});
}

var currentDatabaseVersion = 2;

/*
	Handles credentials loading from and to storage
*/
function LoadCredentials(callback){
	chrome.storage.local.get(['databaseVersion'], function(result) {
		if(result.databaseVersion != null && result.databaseVersion == currentDatabaseVersion) {
			chrome.storage.local.get([
				'username',
				'password'],
				function(result) {
					if(result.username != null && result.password != null){
						_username = result.username;
						_password = result.password;
						callback(true);
					}
					else{
						callback(false);
					}
			   });
		}
		else {
			chrome.storage.local.clear(function(result){});
			callback(false);
		}
	});
}

function SaveCredentials(username, password){
	chrome.storage.local.set({"databaseVersion": currentDatabaseVersion, "username": username, "password" : password}, function(){});
}
/*
	Function will check the credentials with the API. The callback will get called with the return success true/false
*/
function FioCheckCredentials(fnar_username, fnar_password, callback)
{
    var data = 
	{
        "UserName": fnar_username,
        "Password": fnar_password
    };

	var fnar_url = "https://rest.fnar.net";
    var url = fnar_url + "/auth/login";
    var fnarhttp = new XMLHttpRequest();

    fnarhttp.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            var status = this.status;
            if (status === 0 || (status == 200))
            {
				callback(true);
            }
			else
			{
				callback(false);
			}	
        }
    };
    fnarhttp.withCredentials = false;
    fnarhttp.open("POST", url, true);
    fnarhttp.setRequestHeader("Content-type", "application/json");
    fnarhttp.send(JSON.stringify(data));
}