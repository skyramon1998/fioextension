// Handle HTML5 PushState--this is like a half-refresh
browser.webNavigation.onHistoryStateUpdated.addListener(function(details) {
  chrome.tabs.executeScript(null, {file:"contentscript.js"}, 
  _=>{
    let e = chrome.runtime.lastError;
    if(e !== undefined){
      console.log(e);
    }
  });
});

var bHasPromptedRegister = false;

var backgroundPort;

chrome.runtime.onConnect.addListener(function(port) {
	backgroundPort = port;
	console.log("FIO: BackgroundPort Connected");
	
	backgroundPort.onMessage.addListener((msg) => {
		if (msg.command == "DisplayBadge") {
			chrome.browserAction.setIcon({path: "assets/fio-48.png"});
		}
		else if(msg.command == "HideBadge") {
			chrome.browserAction.setIcon({path: "assets/fio-48-dim.png"});
		}
		else if(msg.command == "Register") {
			if (!bHasPromptedRegister) {
				bHasPromptedRegister = true;
				chrome.tabs.create({ url: "https://fio.fnar.net/register?UserName=" + msg.UserName + "&RegistrationGuid=" + msg.RegistrationGuid, active:false }, function(tab) {
					chrome.tabs.update(tab.id, {active:true});
				});
			}
		}
	});
});

